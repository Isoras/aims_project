\chapter{Fundamentals of Relativistic Cosmology }
We review the basics of general relativity, mainly the Einstein field equation, which we then use to get  an idea about the dynamics of the universe expansion.
\section{A Bit of General Relativity}

Newton's mechanics does account for most of the common particle motions. However, when particles move at speeds close to that of light, it breaks down and then the theory of special relativity (SR), as proposed by Einstein in 1905, takes over. The theory of special relativity relies essentially on its postulates that the form of equations of the fundamental physics laws stays invariant under transitions between inertial reference frames and that the speed of light is the maximum speed there is and is invariant or constant in all inertial frames. The special relativity rejects thus the common sense constancy of space, time and  mass measurement. It associates space and time into a 4-dimensional object called spacetime \citep{Inverno}.  In this spacetime, any event is specified by four numbers, three spatial coordinates and time. The path followed by a particle in spacetime is called the worldline $x^{\mu}$, with $\mu=0,1,2,3$. The most important result of special relativity is the famous Einstein mass-energy equation $E=mc^2$ \citep{Tom}. The theory of special relativity is nevertheless valid in a flat spacetime (Minkowski space), a space of zero curvature or equivalently a space where gravity is absent. 

Indeed, in ten years' time, Einstein extended his theory of special relativity to a general theory of gravity known as General Relativity (GR) applicable to nonflat spacetime and which, by the correspondence principle, yields the Newtonian theory and the special theory under the right conditions \citep{Carrol}. In this section, we want to explore the very basic ideas underlying the general theory of gravity of Einstein. The first idea is that spacetime is modelled as a curved four-dimensional mathematical object called pseudo-Riemannian manifold or Lorentzian manifold \citep{Inverno}. In this spacetime, the laws of physics must be independent of the choice of coordinate system used to label points. This explains why general relativity is done using tensor formalism. The second idea is known as the strong equivalence principle of Eintein. According to this principle, at any spacetime point in an arbitrary gravitational field there is a “locally inertial” coordinate system in which the effects of gravitation are absent in a sufficiently small spacetime neighborhood of that point \citep{Steven}. This principle makes general relativity an extension of special relativity to curved spacetime. Lastly,  general relativity regards gravity as an inherent property of curved spacetime or a manifestation of curvature in the geometry of spacetime where matter and momentum flux curves spacetime as described by the Einstein field equation \citep{Carrol}.

At this level it should be no surprise that the famous Einstein field equation is central in general relativity. Our next effort is to understand what it says. This equation is written as
\begin{align}\label{eq:eq21-1}
G_{\mu\nu}={8 \pi G \over c^4} T_{\mu\nu},
\end{align}
where $G_{\mu\nu}$ is the Einstein tensor, $G$ is the Newton's gravitational constant, $c$ is the speed of light and $T_{\mu\nu}$ is the energy-momentum tensor. We can write the same equation as
\begin{align}\label{eq:eq21-2}
R_{\mu\nu} - {1 \over 2}g_{\mu\nu}\,R = {8 \pi G \over c^4} T_{\mu\nu},
\end{align}
where the Einstein tensor has been replaced by its value in terms of the Ricci tensor $R_{\mu\nu}$, the Ricci scalar $R$ and the metric $g_{\mu\nu}$.

We need however to try to understand the mathematics of equation \eqref{eq:eq21-2} for us to fully appreciate what it actually tells us. In what follows we want to understand the mathematical objects that appear in \eqref{eq:eq21-2}.

\subsection{The Metric} 
Let us consider that a clock ticks once every time interval $ds$ when it is a rest in the absence of a gravitational field. Then, if the clock is moving in absence of a gravitational field, the spacetime separation between successive ticks is
\begin{align}\label{eq:eq21-3}
d\tau^2 =-ds^2=-cdt^2+dx^2+dy^2+dz^2.
\end{align}
Introducing the summation convention according to which repeated indices are summed over, we can write
\begin{align}\label{eq:eq21-4}
d\tau^2 =-ds^2=\eta_{\mu \nu}dx^{\mu}dx^{\nu}.
\end{align}
In \eqref{eq:eq21-4} $\eta_{\mu \nu}$ is the Minkowskian metric given by
\begin{align}\label{eq:eq21-5}
\eta_{\mu \nu}=diag
\begin{pmatrix}
-c&1&1&1
\end{pmatrix}.
\end{align}
We notice that \eqref{eq:eq21-3} defines the interval between events in special relativity and  components $\eta_{\mu \nu}$ of the Minkowski metric do not depend on coordinates. If now we consider the clock moving in a general gravitational field, then the interval between ticks is given by
\begin{align}\label{eq:eq21-6}
d\tau^2 =-ds^2=g_{\mu \nu}(x)dx^{\mu}dx^{\nu},
\end{align}
where our new metric $g$ not only depends on coordinates but also has two more properties \citep{Steven}. First, the transition of coordinates $x^{\mu}$ to $x^{\mu^{\prime}}$ transfoms the metric into
\begin{align}\label{eq:eq21-7}
g_{\rho^{\prime}\sigma^{\prime}}=g_{\mu \nu}\frac{\partial x^{\mu}}{\partial x^{\rho^{\prime}}}\frac{\partial x^{\nu}}{\partial x^{\sigma^{\prime}}},
\end{align}
where $x^{\mu}$ and $x^{\mu^{\prime}}$ are the coordinates of the same physical points in different coordinate systems. Second, in inertial local coordinates, $g_{\mu\nu}(x)$ reduces to $\eta_{\mu\nu}$ and its first derivatives vanish.
Since $x^{\mu}$ transfoms as
\begin{align}\label{eq:eq21-8}
dx^{\nu ^{\prime}}=\frac{\partial x^{\nu ^{\prime}}}{\partial x^{\mu}}dx^{\mu},
\end{align} 
equation \eqref{eq:eq21-7} is covariant.

The tensor  $g_{\mu \nu}$ has an inverse $g^{\nu \alpha}$ such that
\begin{align}\label{eq:eq21-9}
g_{\mu \nu}g^{\nu \alpha}=\delta_{\mu}^{\alpha}.
\end{align}
Quantities that transform like \eqref{eq:eq21-8} and \eqref{eq:eq21-7} are called contravariant vector and covariant tensor respectively. A scalar $\phi(x)$ is a quantity that stays the same under coordinate transformation. The derivative of such a quantity is a covariant vector $v_{\mu}$
\begin{align}\label{eq:eq21-10}
v_{\mu}=\frac{\partial \phi(x)}{\partial x^{\mu}},
\end{align}
and it transforms as
\begin{align}\label{eq:eq21-11}
v_{\nu^{\prime}}\equiv \frac{ \partial \phi}{x^{\nu^{\prime}}}=\frac{\partial\phi}{x^{\mu}}\frac{x^{\mu}}{x^{\nu^{\prime}}}=v_{\mu}\frac{x^{\mu}}{x^{\rho^{\prime}}}.
\end{align}
Contravariant tensors have raised indices whereas covariant tensors have lowered indices. In general, we can use the metric to raise and lower indices. For  example, $S^{\beta}=g^{\mu\beta}S_{\mu}$.

\subsection{Geodesic Equation and Affine Connection}

In general relativity, the worldline of a particle that is moving in absence of any external nongravitational force is the geodesic, which is the straightest line there is in curved spacetime. Consider a curve $x^{\nu^{\prime}}$ parametrised by $\lambda$. A tangent vector on this curve is given by
\begin{align}\label{eq:eq21-13}
\frac{d x^{\nu^{\prime}}}{d\lambda}=\frac{\partial x^{\nu^{\prime}}}{\partial x^{\mu}}\frac{d x^{\mu}}{d \lambda}.
\end{align}
By definition, a straight line is a curve that parallel-transports its tangent (the tangent can be moved around on the curve without changing its size and direction). Therefore, if our curve $x^{\nu^{\prime}}$ is a geodesic or the ``straight'' line on the curved spacetime, the second derivative of the vector \eqref{eq:eq21-13} as computed  below must vanish,
\begin{align}\label{eq:eq21-14a}
\frac{d^2 x^{\nu^{\prime}}}{d\lambda ^2}=\frac{d}{d\lambda}\left(\frac{\partial x^{\nu^{\prime}}}{\partial x^{\mu}}\frac{d x^{\mu}}{d \lambda} \right)=\frac{\partial x^{\nu^{\prime}}}{\partial x^{\mu}}\frac{d ^2 x^{\mu}}{d \lambda ^2}+\frac{\partial ^2 x^{\nu^{\prime}}}{\partial x^{\mu} \partial x^{\sigma}}\frac{d x^{\mu}}{d \lambda }\frac{d x^{\sigma}}{d \lambda}=0.
\end{align}
But there is a problem!  What we have got is not a vector because of the the second term. Moreover, \eqref{eq:eq21-14a} is not covariant. We need a correction to make it covariant. We introduce a nontensor quantity ${\Gamma^{\sigma}} _{\mu\nu}$ which will cancel the second term. It is called the affine connection, whose  general transformation law is
\begin{align}\label{eq:eq21-11}
{\Gamma^{ {\alpha}^{\prime}}}_{{\sigma}^{\prime}{\rho}^{\prime}}=\frac{\partial x^{{\alpha}^{\prime}}}{\partial x^{\tau}}\frac{\partial x^{\mu}}{\partial x^{{\sigma}^{\prime}}}\frac{\partial  x^{\nu}}{\partial x^{{\rho}^{\prime}}}{\Gamma^{\tau}}_{\mu \nu}-\frac{\partial ^2x^{{\alpha}^{\prime}}}{\partial x^{\mu}\partial x^{\nu}}\frac{\partial x^{\mu}}{\partial x^{{\sigma}^{\prime}}}\frac{\partial x^{\nu}}{\partial x^{{\rho}^{\prime}}}.
\end{align}
The corrected equation of motion for the particle along its geodesic which is covariant becomes
\begin{align}\label{eq:eq21-12}
\frac{d^2x^{\tau}}{d\lambda^2}+{\Gamma^{\tau}}_{\mu \nu}\frac{dx^{\mu}}{d\lambda}\frac{dx^{\nu}}{d\lambda}=0.
\end{align}
This is the geodesic equation. In a coordinate system which is locally inertial and cartesian at $x$, the affine connection ${\Gamma^{\tau}}_{\mu \nu}$ also known as Christofell symbol vanishes, which leads to the correct  equation of motion in absence of gravitational field as $d^2x^{a}/d\lambda^2=0$. We therefore define the covariant derivative of a tensor as
\begin{align}\label{eq:eq21-12}
\nabla _{\alpha} {T^{\mu}}_{\nu} =\partial _{\alpha} {T^{\mu}}_{\nu} + {\Gamma^ {\mu}} _{\alpha \beta} {T^{\beta}}_{\nu}-{\Gamma ^{\beta}}_{\nu \alpha} {T^{\mu}}_{\beta}.
\end{align}
The affine connection can be expressed in terms of the metric through
\begin{align}\label{eq:eq21-14}
{\Gamma^{\alpha}} _{\mu\nu}=\frac{g^{\alpha \beta}}{2}\left(\frac{\partial g_{\beta \nu}}{\partial x^{\mu}}+\frac{\partial g_{\beta \mu}}{\partial x^{\nu}}-\frac{\partial g_{\mu \nu}}{\partial x^{\beta}} \right).
\end{align}

\subsection{Riemann Tensor}
The curvature tensor or Riemann-Christoffel tensor or Riemann tensor is defined by \citep{Bernard,Inverno}
\begin{align}\label{eq:eq21-17}
{R^{\beta}}_{\mu \nu \alpha}=\partial _{\nu} {\Gamma ^{\beta}}_{\mu \alpha}-\partial _{\alpha} {\Gamma^{\beta}} _{\mu\nu}+{\Gamma^{\sigma}} _{\mu \alpha}{\Gamma ^{\beta}}_{\sigma \nu}-{\Gamma^{\sigma}} _{\mu\nu}
{\Gamma ^{\beta}}_{\sigma \alpha}.
\end{align}
Looking at \eqref{eq:eq21-14} and \eqref{eq:eq21-17}, we notice that the curvature tensor depends on the metric and its first and second derivatives. The contraction of the Riemann tensor leads to Einstein tensor $G_{\mu\nu}$. Let us first obtain the Ricci tensor $R_{\mu \nu}$ and the Ricci scalar $R$ respectively as
\begin{align}\label{eq:eq21-18}
R_{\mu\nu}={R^{\alpha}}_{\mu\alpha\nu}=g^{\alpha\beta}R_{\beta \mu \alpha \nu}
\end{align}
and
\begin{align}\label{eq:eq21-19}
R=g^{\mu\nu}R_{\mu\nu}.
\end{align}
The Einstein tensor is then written as
\begin{align}\label{eq:eq21-20}
G_{\mu\nu}=R_{\mu\nu}-\frac{1}{2}g_{\mu\nu}R.
\end{align}
\subsection{Energy-Momentum Tensor and Einstien Field  Equation}
The energy-momentum tensor describes the  density and  flux of energy and momentum in spacetime. It is the source of gravitational field in general relativity. Let us first consider a matter field of non-interacting incoherent matter or dust. To characterize this matter, we need only the 4-velocity $u^{\mu}=dx^{\mu}/d\tau $ of flow and the proper density $\rho_0=\rho_0(x^{\mu})$ as measured by a co-moving observer. The energy-momentum tensor for such a system is written as
\begin{align}\label{eq:eq21-27}
T^{\mu \nu}=\rho_0u^{\mu}u^{\nu}.
\end{align}
In special relativity, the zero-zero component gives the density $\rho$ as measured by a fixed observer,
\begin{align}\label{eq:eq21-28}
T^{00}=\rho_0\frac{dx^0}{d\tau}\frac{dx^0}{d\tau}=\rho_0\frac{dt^2}{d\tau^2}=\gamma^2\rho_0=\rho.
\end{align}
We can show the components of $T^{\mu\nu}$ as
\begin{align}\label{eq:eq21-29}
T^{\mu\nu}=\rho
\begin{pmatrix}
1&u_x&u_y&u_z\\
u_x&u^2_x&u_xu_y&u_xu_z\\
u_y&u_xu_y&u^2_y&u_yu_z\\
u_z&u_xu_x&u_yu_z&u^2_z
\end{pmatrix}.
\end{align}
The equation of motion of a matter field of dust in flat space is
\begin{align}
\partial_{\nu}T^{\mu\nu}=0.
\end{align}
Let's notice that with $\mu =0$, it reduces to the classical equation of continuity. In general relativity where spacetime is nonflat the law of conservation looks like $\nabla _{\nu}T^{\mu\nu}=0$. Unlike incoherent matter, characterization of a perfect fluid will require one more quantity, the scalar pressure $P=P(x^{\mu})$ and the energy-momentum tensor becomes
\begin{align}\label{eq:eq21-32}
T^{\mu\nu}=\left(\rho_0 + P \right)u^{\mu}u^{\nu}-Pg^{\mu\nu}.
\end{align}

We can now paraphrase the information contained in the Einstein field equation (EFE). The left hand side of \eqref{eq:eq21-1} or \eqref{eq:eq21-2} describes the curvature of spacetime as determined by the metric whereas the right hand side of the same equation describes the energy/momentum content of the spacetime. Our tensorial equation is in fact a set of ten second-order partial differential equations for the ten independent components of the metric tensor with the components of the energy-momentum tensor as the source terms. The solutions of these equations are called the metrics of spacetime, $ds^2$. In fact, given a distribution of matter and energy as the energy-momentum tensor, we can determine the geometry of spacetime by providing the definition of distance.

The Einstein field equation has many particular solutions \citep{Inverno}. The Minkowskian metric is the simplest solution that describes the flat spacetime in special relativity. The Schwarzschild metric describes the geometry around a spherical nonrotating mass such as a star. The Kerr metric describes a rotating black hole. There is a solution which describes gravitational waves as well. Most importantantly for us, the Friedmann-Robertson-Walker metric describes the spatially homogeneous and isotropic expanding universe. In fact the latter solution is the centrepiece for the next section.

\section{General Relativistic Cosmology}

Relativistic cosmology that is the object of this section has its basis in three assumptions. The first is the cosmological principle as stated in the introductory chapter. The second is the general relativity as we said in the introduction also and this is why the first section of this chapter is dedicated to some general relativity basics. The third assumption is the Weyl's postulate. Weyl introduced the concept of substratum, a fluid pervading space in which galaxies move like fundamental particles \citep{Inverno}. The postulate then states that the particles in the substratum lie in spacetime on a congruence of timelike geodesics diverging from a common point \citep{Inverno}.
\subsection{Robertson-walker (RW) Metric}
The cosmological principle of invariance under rotation (isotropy) and invariance under translation (homogeneity) that is assumed on large scales turns out to hold only in space, not in time. This implies that spacetime is not maximally symmetric since the universe is expanding in time. Therefore, the metric for an expanding universe will look like
\begin{align}\label{eq:EQ22-}
ds^2=-dt^2+R^2(t)d\sigma^2,
\end{align}
where $R(t)$ is a function called the scale factor, and $d\sigma^2$ is the metric on a maximally symmetric three-manifold. Here, maximal symmetry is just another way of saying spatial homogeneity and isotropy. We can write $d\sigma^2=\gamma _{ij}(x)dx^idx^j$. The Riemann tensor for the maximally symmetric 3-manifold is given by
\begin{align}
R_{ijkl}=k\left(\gamma_{ik}\gamma_{jl}-\gamma_{il}\gamma_{jk} \right),
\end{align}
which can be contracted to the Ricci tensor
\begin{align}\label{eq:EQ22-5a}
R_{jl}=2k\gamma_{jl}.
\end{align}
Maximal symmetry implies  spherical symmetry, $d\sigma ^2$ can therefore be conveniently written using spherical coordinates, 
\begin{align}
d\sigma ^2=dr^2+r^2(d\theta ^2+\sin ^2\theta d\phi ^2)=dr^2+r^2 d\Omega ^2.
\end{align}
Furthermore, we can multiply each term by an exponential function of the radial coordinate $q$ which will not disturb our spherical symmetry (the form of $d\Omega^2$ is preserved). We can thus write
\begin{align}\label{eq:EQ22-7}
d\sigma^2=\gamma _{ij}(x)dx^idx^j=e^{2f(q)}dq^2+q^2d\Omega^2,
\end{align}
where the metric tensors for $d\sigma ^2$ now look like
\begin{align}\label{eq:EQ22-8}
\gamma_{ij}=diag
\begin{pmatrix}
e^{2f}&q^2&q^2\sin^2\theta
\end{pmatrix};\nonumber \\
\gamma^{ij}=diag
\begin{pmatrix}
e^{-2f}&\frac{1}{q^2}&\frac{1}{q^2\sin^2\theta}
\end{pmatrix}.
\end{align}
The nonvanishing components of the Ricci tensor for this static , spherically symmetric 3-manifold are
\begin{align}\label{eq:EQ22-9}
R_{qq}=\frac{2}{q}\partial _q f;\quad
R_{\theta \theta}= e^{-2f}(q\partial _qf-1)+1;\quad
R_{\phi \phi}= \left( e^{-2f}(q\partial _qf-1)+1\right)\sin^2\theta.
\end{align}
Equating \eqref{eq:EQ22-9} to \eqref{eq:EQ22-5a} and using \eqref{eq:EQ22-8} we solve for $f$ and we get
\begin{align}\label{eq:EQ22-10}
f=-\frac{1}{2}\ln(1-kq^2).
\end{align}
Using \eqref{eq:EQ22-10} in \eqref{eq:EQ22-7}, we can write the metric for our 3-manifold as
\begin{align}\label{eq:EQ22-11}
d\sigma^2=\frac{dq^2}{1-kq^2}+q^2d\Omega^2,
\end{align}
where the curvature $k$ can take three values $0$ for flat surface, $-1$ for open surface or $1$ for closed surface.
We finally get the Robertson-Walker metric, a metric on spacetime which describes a maximally symmetric surface evolving in size (expanding), as
\begin{align}\label{eq:EQ22-12}
ds^2=-dt^2+R^2(t)\left[ \frac{dq^2}{1-kq^2}+q^2d\Omega^2\right].
\end{align}
In \eqref{eq:EQ22-12} the scale factor has units of distance and the radial coordinate $q$ is dimensionless. We want a dimensionless scale factor $a(t)$, coordinate $r$ with distance dimensions and a curvature parameter $\kappa$ with dimensions $(length)^{-2}$. We use a quantity $R_0$ with distance dimensions and we set
\begin{align}\label{eq:	EQ22-13}
a(t)=\frac{R(t)}{R_0}, \quad r=R_0q \quad\text{and}\quad \kappa =\frac{k}{R^2_0}.
\end{align} 
Using the new variables the Robertson-Walker metric becomes
\begin{align}\label{eq:EQ22-14}
ds^2=-dt^2+a^2(t)\left[ \frac{dr^2}{1-kr^2}+r^2d\Omega^2\right],
\end{align}
whose metric tensors now look like
\begin{align}\label{eq:EQ22-15}
g_{\mu\nu}=diag
\begin{pmatrix}
-1&\frac{a^2}{1-\kappa^2}&a^2r^2&a^2r^2\sin^2\theta
\end{pmatrix};\nonumber \\
g^{\mu\nu}=diag
\begin{pmatrix}
-1&\frac{1-\kappa^2}{a^2}&\frac{1}{a^2r^2}&\frac{1}{a^2r^2\sin^2\theta}
\end{pmatrix}.
\end{align}
Similarly to the 3-manifold case above, we can now evaluate the Christofell symbols, the Riemann and Ricci tensors and then find the Ricci scalar. 
The nonvanishing components of the Ricci tensor are
\begin{align}\label{eq:EQ22-16}
R_{00}&=R_{tt}=-3\frac{\ddot{a}}{a},\quad  R_{11}=R_{rr}=\frac{a\ddot{a}+2\dot{a}^2+2\kappa}{1-\kappa r^2},\quad R_{22}=R_{\theta \theta}=r^2\left(a\ddot{a}+2\dot{a}^2+2\kappa \right)\nonumber\\
\text{and}&\quad R_{33}=R_{\phi \phi}=r^2\left(a\ddot{a}+2\dot{a}^2+2\kappa \right)\sin^2\theta;
\end{align}
and the Ricci scalar is found to be
\begin{align}\label{eq:EQ22-17}
R=6\left[\frac{\ddot{a}}{a}+ \left(\frac{\dot{a}}{a}\right)^2+\frac{\kappa}{a^2}\right].
\end{align}
\subsection{Friedmann Equations}
This equation describes the behaviour of the scale factor. We use the Ricci tensor components \eqref{eq:EQ22-16} and the Ricci scalar \eqref{eq:EQ22-17} in the Einstein equation \eqref{eq:eq21-2}. Following Weyl's postulate, we will model matter and energy by an isotropic perfect fluid which is at rest in a comoving frame. For such a fluid, the  the 4-velocity is given by
\begin{align}\label{eq:EQ22-18}
u^{\mu}=(1,0,0,0).
\end{align}
The energy-momentum tensor defined by equation \eqref{eq:eq21-32} becomes
\begin{align}
{T^{\mu}} _{\nu}=diag
\begin{pmatrix}
-\rho&P&P&P
\end{pmatrix},
\end{align}
where $P$ is the pressure of the isotropic perfect fluid.
The $00$-component of Einstein equation reduces thus to
\begin{align}\label{eq:EQ22-19}
\left(\frac{\dot{a}}{a}\right)^2=\frac{8\pi G}{3}\rho -\frac{c^2\kappa}{a^2},
\end{align}
and the $ii$-component gives
\begin{align}\label{eq:EQ22-20}
\frac{\ddot{a}}{a}=-\frac{4\pi G}{3}\left(\rho +\frac{3P}{c^2}\right).
\end{align}
Equations \eqref{eq:EQ22-19} and \eqref{eq:EQ22-20} are known as the Friedmann equations. We can write Friedmann equations in natural units ($c=1$) as
\begin{align}\label{eq:EQ22-21}
\left(\frac{\dot{a}}{a}\right)^2=\frac{8\pi G}{3}\rho -\frac{\kappa}{a^2},
\end{align}
and
\begin{align}\label{eq:EQ22-22}
\frac{\ddot{a}}{a}=-\frac{4\pi G}{3}\left(\rho +3P\right).
\end{align}
Specifically, \eqref{eq:EQ22-21} is known as Friedmann equation and \eqref{eq:EQ22-22} is known as second Friedmann equation or acceleration equation. 

We cannot yet use Friedmann equations unless we have an equation of time evolution of the density. As we said earlier, the vanishing covariant derivative of ${T^{\mu}} _{\nu}$ expresses the conservation of matter and energy in spacetime. Let's write down the $\nu=0$ component of this derivative using \eqref{eq:eq21-12},
\begin{align}\label{eq:EQ22-xa}
\frac{\partial {T^{\mu}}_0}{\partial x^{\mu}}+{\Gamma ^{\mu}}_{\alpha \mu}{T^{\alpha}}_0-{\Gamma ^{\alpha}}_{0\mu}{T^{\mu}}_{\alpha}=0.
\end{align}
In \eqref{eq:EQ22-xa}, only ${T^i}_i$ survive and it turns out that $\mu=0$ in the first and $\alpha=0$ in the second terms, which gives us
\begin{align}
-\frac{\partial \rho}{\partial t}-\rho {\Gamma ^{\mu}}_{0 \mu}-{\Gamma ^{\alpha}}_{0\mu}{T^{\mu}}_{\alpha}=0.
\end{align}
The Christofell symbols which do not vanish are ${\Gamma ^1}_{0 1}={\Gamma ^2}_{0 2}={\Gamma ^3}_{0 3}=\dot{a}/a$, the conservation law in an expanding universe thus reduces to
\begin{align}\label{eq:EQ22-xb}
\dot{\rho}+3\frac{\dot{a}}{a}\left(\rho + P \right)=0.
\end{align}
We could use the first law of thermodynamics and consider a reversible expansion ($dS=0$) in which energy is $E=mc^2$ and still get \eqref{eq:EQ22-xb} which is known as the fluid equation \citep{Liddle}. Equation \eqref{eq:EQ22-xb} is completed by the specification of the universe energy-matter content given as the equation of state $P=P(\rho)=w\rho$ which, in the case of flat universe, can lead to three cases
\begin{align}
a(t)&=\left(\frac{t}{t_0} \right)^{2/3},\quad \rho_m (t)=\frac{\rho_0}{a^3}=\frac{\rho_0t_0^2}{t^2};\nonumber\\
a(t)&=\left(\frac{t}{t_0} \right)^{1/2},\quad \rho_r (t)=\frac{\rho_0}{a^4}=\frac{\rho_0t_0^2}{t^2};\nonumber\\
\rho_{\Lambda} & = const.
\end{align}
for a matter-dominated or pressureless ($w=0$) universe,  a radiation-dominated universe ($w=1/3$) and a vacuum energy- dominated universe ($w=-1$) universe, respectively. Solutions can also be found for $\kappa=\pm 1$ cases and cases where we have a mixture of matter,  radiation and vacuum energy (also cosmological constant) \citep{Liddle}. Here, by matter and radiation we mean nonrelativistic matter and relativistic particles respectively.

\section{Basic Cosmological Parameters}

\subsection{Redshift}
An electromagnetic radiation is said to be redshifted when the observed wavelength is greater than that emitted by the source. As invoked earlier in the introduction, redshifts of galaxies show that they moving away from us. We define the redshift $z$ in terms of wavelength $\lambda$ as
\begin{align}
1+z=\frac{\lambda_{obs}}{\lambda_{emit}}.
\end{align}
Let us consider the Robertson-Walker metric \eqref{eq:EQ22-14}  for light propagating radially from $r=0$ to $r=r_0$. Since light does not travel any distance in spacetime, meaning that the light travel geodesics with $ds=0$, we can write
\begin{align}
c\frac{dt}{a(t)}=\frac{dr}{\sqrt{1-\kappa r^2}}.
\end{align}
Therefore, to travel from $r=0$ to $r=r_0$ light takes the time
\begin{align}
c\int_{t_{emit}}^{t_{obs}}\frac{dt}{a(t)}=\int_{0}^{r_0}\frac{dr}{\sqrt{1-\kappa r^2}}.
\end{align}
Then, since in comoving coordinates galaxies remain at fixed positions, considering a light signal emitted later at $t_{emit}+dt_{em}$ and so observed at $t_{obs}+dt_{obs}$ gives the integral
\begin{align}
c\int_{t_{emit}}^{t_{obs}}\frac{dt}{a(t)}=c\int_{t_{emit}+dt_{emit}}^{t_{obs}+dt_{obs}}\frac{dt}{a(t)}.
\end{align}
This leads to 
\begin{align}
\frac{dt_{obs}}{a(t)}=\frac{dt_{emit}}{a(t)}
\end{align}
which leads further to \citep{Liddle}
\begin{align}
1+z=\frac{\lambda_0}{\lambda}=\frac{a(t_0)}{a_(t)}.
\end{align}
In an expanding universe, we can obviously see that $z$ will be greater for light emitted closer to the big bang. This allows cosmologists to use the redshift to describe various epochs of the universe history. 

\subsection{Hubble, Density and Deceleration Parameters}

Hubble law says the recessional velocity $\bold{v}$ of a galaxy is proportional to the physical distance $\bold{r}$, or mathematically,
\begin{align}
\bold{v}=H(t)\bold{r}=\frac{\dot{a}}{a}\bold{r},
\end{align}
where the Hubble parameter $H(t)=\dot{a}/a$ is the rate of expansion of the universe. The Hubble parameter is constant in space but evolves in time. Its present value (Hubble constant) is $H_0=100h \text{km}/\text{sec}/\text{Mpc}$ with $h\sim 0.673\pm 0.012$ \citep{Planck}. The physical distance is given by $\bold{r}=a\bold{x}$, where $\bold{x}$ is the comoving distance assumed constant.


The critical density $\rho _c$ given by
\begin{align}
\rho_c(t)=\frac{3H^2}{8\pi G},
\end{align}
is obtained by setting $\kappa =0$ in \eqref{eq:EQ22-21}. It is the total density required to make the geometry of the universe flat. Its value today is $\rho_c(t_0)=1.88h^2\times 10^{-26}\,\text{kg}\,\text{m}^{-3}=2.78h^{-1}\times 10^{11}\,M_{\odot}/\left( h^{-1}\,\text{Mpc}\right)^3$ \citep{Liddle}. We define a dimensionless quantity $\Omega(t)$, the parameter density, by
\begin{align}
\Omega(t)\equiv \frac{\rho}{\rho_c}.
\end{align}
When used in the Friedmann equation, it makes it look like
\begin{align}\label{eq:EQ}
\Omega-1=\frac{\kappa}{a^2H^2}.
\end{align}
If we set the right hand side of \eqref{eq:EQ} equal to $-\Omega_{\kappa}$, where $\Omega_{\kappa}$ is the density parameter associated to the universe curvature, then we can rewrite the Friedmann equation as
\begin{align}\label{eq:wq}
\Omega +\Omega_{\kappa}=1.
\end{align}

Here we need to understand that the energy density $\rho$ as it appears in the Friedmann equation is a summed contribution from various components (see chapter one). Therefore, we refer to the density parameter defined above  as $\Omega_{tot}$, the sum of fractional density parameters, $\Omega_{rad}$ for radiation and relativistic matter, $\Omega_b$ for baryonic matter, and $\Omega_{dm}$ for dark matter and $\Omega_{\Lambda}$ for dark energy or vacuum energy or  cosmological constant $\Lambda$. Or, mathematically,
\begin{align}
\Omega_{tot}=\Omega_{rad}+\Omega_b + \Omega_{dm}+\Omega_{\Lambda}=\Omega_r+\Omega_m+\Omega_{\Lambda},
\end{align}
where $\Omega_m$ stands for both baryonic and dark matter. Observations have shown that today, while $\Omega_r = 8.4\times 10^{-5}$ (negligibly small contribution from photons and neutrinos), $\Omega_m=0.315\pm 0.017$, $\Omega_{\Lambda}=0.6825$, $\Omega_{\kappa}=-0.0326$, $\Omega_bh^2=0.02205\pm 0.00028$ and $\Omega_{dm}h^2=0.1199\pm 0.0027$, ($\Omega_mh^2=1.4300$) \citep{Planck}. More importantly, $\Omega_{tot}$ is very close to $1$, which suggests that the universe total density is nearly critical. In other words, our universe is nearly flat \citep{Pilar}.

We now talk about another parameter, deceleration parameter $q_0$ which we obtain by taking the Taylor expansion of the scale factor $a(t)$ about the present time $t_0$ and then divide by $a(t_0)$. This is
\begin{align}
a(t)=a(t_0)+(t-t_0)\dot{a}(t_0)+\frac{1}{2}(t-t_0)^2\ddot{a}(t_0)+\cdots.
\end{align}  
We then divide by $a(t_0)$ to get
\begin{align}
\frac{a(t)}{a(t_0)}=1+(t-t_0)H_0-\frac{1}{2}(t-t_0)^2H^2_0q_0,
\end{align}
where our deceleration parameter is
\begin{align}
q_0=-\frac{1}{H_0^2}\frac{\ddot{a}(t_0)}{a(t_0)}.
\end{align}
Using equations the second Friedmann equation (acceleration equation) for a pressureless universe and our definition of the critical density, we obtain that 
\begin{align}
q_0=\Omega_{r,0}+\frac{1}{2}\Omega_{m,0}-\Omega_{\Lambda}.
\end{align}
Using the omega values above we get a negative $q_0$ due to the dominant value of $\Omega_{\Lambda}$, meaning $\ddot{a}>0$. Therefore, we conclude that the universe is not only expanding but also the expansion is accelerated.
\subsection{Time and Distances}
In many contexts it is important to define the conformal time $\eta$ by
\begin{align}
\eta\equiv \int_0^t \frac{dt^{\prime}}{a(t^{\prime})},
\end{align}
which is the comoving horizon and therefore regions separated by distances greater than $\eta$ are causally disconnected. The comoving distance or proper distance between us and a distant astronomical object that emits light is
\begin{align}
d_p(t_0)= \int_{t(a)}^{t_0} \frac{dt^{\prime}}{a(t^{\prime})}=\int_a^1 \frac{da^{\prime}}{a^{\prime 2}H(a^{\prime})},
\end{align}
where the scale factor today is $a(t_0)= 1$. In a spatially flat matter-dominated universe, the proper distance is
\begin{align}
d_p(t_0)=\frac{2}{H_0}\left[1-\frac{1}{\sqrt{1+z}}\right].
\end{align}
Since the current proper distance to a galaxy is not measurable, some techniques for measuring distances have been elaborated in cosmology and astronomy. Within our solar system the radar technique is quite useful. The distance between the earth and a celestial body is given  by $d=(1/2)ct$, where $t$ is the time taken by the radar signal sent from the earth to travel to the body and back to the earth after  being reflected by the body. Distances in our galaxy are better determined by the trigonometric parallax method. This method yields 
\begin{align}
d_{\theta}=1pc\left( \frac{b}{1\text{AU}}\right)\left(\frac{\theta}{1\,\text{arcsec}}\right)^{-1}.
\end{align}
In fact, when a star is observed from two points separated by a distance $b$ along a baseline, its position is seen to shift by an an angle $\theta$. It is customary to use the earth's orbit round the sun so that $b=2\text{AU}$. The difficult task is then to measure the angle $\theta$ with sufficient accuracy, which is not always feasible \citep{Ryden}. Luminosity distance is another alternative, it is defined as
\begin{align}
d_{L}=\left(\frac{L}{4\pi f}\right)^{1/2},
\end{align}
where $L$ is the luminosity in watts of a ``standard candle'', an object whose luminosity is known and $f$ in watts per square metres is the radiation flux measured. With our well supported assumption that the universe is nearly flat, it can shown that $d_L=(1+z)d(t_0)$ \citep{Ryden}. Finally, we define the angular-diameter distance. Instead of luminosity, the known size $l$ of a cosmological object can be used to determine its its angular-diameter distance
\begin{align}
 d_A=  \frac{l}{\theta},
\end{align} 
where $\theta$ is the angle it subtends to the eye. Under our usual assumptions, one can show \citep{Liddle, Ryden} that
\begin{align}
d_A(1+z)=d_p(t_0)=d_L/(1+z).
\end{align}
\section{Inflation}
We talked about it in the introduction, here is now a brief account for it. We once again look at the Friedmann equation $|\Omega_{tot}(t)-1|=|\kappa|/a^2H^2$ which tells us that $|\Omega_{tot}(t)-1|\propto a^2\propto t$ during the radiation domination era and $|\Omega_{tot}(t)-1|\propto a \propto t^{2/3}$ during matter domination epoch. In either case $|\Omega_{tot}(t)-1|$ is an increasing function of time, which would imply an unstable flat geometry.  As observed $\Omega_{tot}(t)$ is close to one today, $|\Omega_{tot}(t)-1|\leq 0.2$. In the past at the point of matter-radiation equality, we can show that $|\Omega_{mr}-1|\leq 2\times 10^{-4}$. This shows that the density gets closer and closer to critical as we go back into the past. However, the pure hot big bang model or standard model of cosmology does not provide any explanation for this evidenced situation. It is what we referred to as the flatness problem in the introduction. It can be solved rather easily if we assume that the universe expanded very rapidly in its very early time and then got a high enough flatness level that has been falling so far with the ordinary expansion up to the  still flat situation observed today. It is this extremely rapid expansion ($\ddot{a}(t)>0$) which is known as inflation and it is thought to have taken place when the universe was $10^{-34}\, \text{sec}$ old. Here we can only guess that it has to do with something that has a negative pressure.

In the previous section, the conformal time shows that the universe has a finite age and  light has travelled a finite distance by today which gives rise to a region we call the observable universe. Moreover, the standard model is based on the cosmological principle of universe homogeneity and isotropy and indeed, one example is the thermal equilibrium of the $2.724 \text{K}$ cosmic microwave background radiation (whose origin is discussed in the fourth chapter). Nevertheless, it is not explained how two distant points can be in thermal equilibrium with no prior contact or interaction. This is the horizon problem as invoked in the introduction also. The solution to the puzzle is rather simple if we think again about inflation. In fact, a  patch of the universe  so small that it thermalizes can expand during inflation to a size larger than the observable universe, explaining that two points no matter how distant they are, were once in equilibrium.

The inflationary expansion solves also the problem of relic particle abundances such as magnetic monopole. The dramatic expansion dilutes away these particles so much that their abundance is too faint to detect today. Inflation is currently an active research area but researchers  have not yet understood what caused it.