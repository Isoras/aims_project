

\chapter{ Boltzmann Equation in Cosmology}

In the very early, dense and hot universe, interactions among particles were so frequent that a thermal equilibrium existed. However, departures from equilibrium did happen and resulted in important phenomena including big bang nucleosynthesis, recombination and production of dark matter, photon and neutrino decoupling (becoming free of interactions), inflation, etc. \citep{Dodelson}. In this chapter we want to use the Boltzmann and Saha equations to study the homogeneous big bang nucleosynthesis, primordial recombination, photon decoupling resulting in the cosmic microwave background and free-out of dark matter. So far, We have established that as the universe expands the radiation density, matter density and temperature fall off  as $a^{-4}$,  $a^{-3}$ and $a^{-1}$ respectively. We still need two more pieces of physics to do our job. First, normally a particle can only be formed if its binding energy is greater than the energies of particles it is interacting with. Second, in an expanding universe, interacting particles will remain in equilibrium as long as the interaction rate is greater than the expansion rate. 

\section{Big Bang  Nucleosynthesis}
While heavier elements such as carbon, silicon, sulphur and  iron are formed in the stars as they age in a process known as stellar nucleosynthesis, the fact that young stars seem to start their lives with nonzero abundances of the light elements, helium, hydrogen, deuterium, helium-3 and lithium suggests that these light elements are the components of the primordial gas that was formed in the early universe \citep{Liddle, NASA}.

When the universe has cooled down to a temperature $\sim 1\,\text{MeV}$ , the cosmic plasma comprises two parts, relativistic and nonrelativistic particles. The relativistic particles are the electrons, positrons and photons kept in equilibrium by electromagnetic interactions such as $e^+e^-\leftrightarrow \gamma \gamma$ and the decoupled neutrinos. The nonrelativistic particles are the baryons with survived an initial asymmetry that did exist in the number of baryons and antibaryons. The baryon-to-photon ration is \citep{Dodelson} 
\begin{align}\label{eq:eqx}
\eta_b\equiv n_b/n_{\gamma}=2.75\times 10^{-8}\Omega_b h^2.
\end{align}

Solving \eqref{eq:eq31-23} reveals the fate of the baryons. We make two assumptions. First, above $\sim 0.1\,\text{MeV}$ there exist only free protons and neutrons, no light elements form. Second, when elements start to form, no elements heavier than helium are formed at a significant rate and this is due to the short life time of the neutron as we will see later in this chapter. To start, we examine the formation of the deuterium nucleus in the equation $\text{n}+\text{p}\longleftrightarrow \text{D}+\gamma$.  Realising that $n_{\gamma}=n_{\gamma}^0$ for the photon, \eqref{eq:eq31-24} leads to
\begin{align}\label{eq:eq32-2}
\frac{n_D}{n_nn_p}=\frac{n_D^0}{n_n^0n_p^0}
\end{align}
which, upon using result \eqref{eq:eq31-20}, yields
\begin{align}\label{eq:eq32-3}
\frac{n_D}{n_nn_p}=\frac{3}{4}\left(\frac{2\pi m_D}{m_nm_p}\right)^{3/2}e^{B_D/T},
\end{align}
where the numbers of spin states $g_D=3$ and $g_n=g_p=2$ have resulted into the fraction we can see in the prefactor. Furthermore, we have set $m_D=2\,m_n=2\,m_p$ in the prefactor but kept $m_n+m_P-m_D=B_D$ (deuterium binding energy) in the exponential. Then, realizing that $n_p \propto n_b$ and $n_n \propto n_b$, we obtain a rough estimate
\begin{align}\label{eq:eq32-4}
\frac{n_D}{n_b}\sim \eta_b\left(\frac{T}{m_p}\right)^{3/2}e^{B_D/T}
\end{align} 
which is dominated by the prefactor as long as $B_D/T$ is not too large. The small value of $\eta_b$ thus prevents nuclei formation until the temperature falls well below the binding energy $B_D$.

As it might have been pointed out somewhere above, the abundance of neutrons in the early universe is very crucial to our discussion. We will study it by considering the neutron-proton ratio. In the nonrelativistic limit $E_i=m_i+p_i^2/2m_i$, the proton-neutron equilibrium ratio can be obtained using \eqref{eq:eq31-20} as
\begin{align}\label{eq:eq32-5}
\frac{n_p^0}{n_n^0}=e^{\mathcal{Q}/T},
\end{align}  
where the integrals give the ratio $(\frac{m_p}{m_n})^{3/2}\simeq 1$ and $\mathcal{Q}\equiv m_n-m_p=1.293\text{MeV} $. 


Until $T\sim 1\text{MeV}$, protons can be converted into neutrons via equilibrium weak interactions such as $p+e^-\longleftrightarrow n+\nu_e$. Assuming that for the two leptons (electron and its neutrino), $n_{e^-}=n^0_{e^-}$ and $n_{\nu_e}=n^0_{\nu_e}$, the Saha equation for this interaction results in
\begin{align}
\frac{n_p^0}{n_n^0}=\frac{n_p}{n_n}=e^{\mathcal{Q}/T},
\end{align}
showing that at high enough temperatures protons and neutrons are equally abundant. But, as the temperature drops below $1\,\text{MeV}$ the neutron fraction falls and therefore, if the weak interactions are efficient enough to sustain equilibrium indefinitely, then the neutron fraction would drop to zero \citep{Dodelson}.

However, what happens to the neutron fraction in the real world where weak interactions are not efficient enough to maintain equilibrium indefinitely, is different and that is what we want to deal with.  We conveniently define $X_n$ the ratio of neutrons to the total nucleons as
\begin{align}\label{eq:eq32-6}
X_n\equiv \frac{n_n}{n_n+n_p}
\end{align} 
which, in equilibrium, reduces to
\begin{align}\label{eq:eq32-7}
X_{n, eq}\equiv \frac{n_n^0}{n_n^0+n_p^0}=\frac{1}{1+(n_p^0/n_n^0)}.
\end{align} 
As a start let us now consider \eqref{eq:eq31-23}, with a  and c being the neutron and proton respectively, and b and d are the leptons in equilibrium. Also, we assume that $n_l=n_l^0$. This leads to
\begin{align}\label{eq:eq32-8}
\frac{dn_n}{dt}+3Hn_n=n_l^0\langle \sigma v\rangle\left\lbrace \frac{n_pn_n^0}{n_p^0}-n_n\right\rbrace.
\end{align} 
Using \eqref{eq:eq32-6} and \eqref{eq:eq32-7} into \eqref{eq:eq32-8} we get
\begin{align}\label{eq:eq32-9}
\frac{dX_n}{dt}=\lambda_{np}\left\lbrace (1-X_n)e^{-\mathcal{Q}/T}-X_n \right\rbrace,
\end{align}
where $\lambda_{np}=n_l^0\langle \sigma v\rangle$ is the rate for neutron-to-proton conversion. Let us change variable by using $x=\mathcal{Q}/T$. Both $T$ and $\lambda_{np}$ are function of time. Moreover, the temperature in the expanding universe scales as $a^{-1}$. This can lead us to $dx/dt=-x(dT/T)=Hx$. Since nucleosynthesis occurs in the early radiation-dominated universe, the major part of energy comes from relativistic particles. These are photons $g_{\gamma}=2$, six flavours of neutrinos $g_{\nu}=6$, and electrons and positrons $g_{e^-}=g_{e^+}=2$ \citep{Dodelson, Turner}. Considering that all these particles have the same temperature, we can get $g_{\ast}=10.75$ giving us $\rho=10.75\pi^2T^4/30 $. Furthermore, the radiation energy density scales as $T^4$ \eqref{eq:eqr} and Friedmann equation tells thus that $H\propto T^2$. This allows us to write $H=H_1/x^2$, where $H_1$ is the Hubble parameter at $T=\mathcal{Q}$ or $x=1$. We change \eqref{eq:eq32-9} to
\begin{align}\label{eq:eq32-10}
\frac{dX_n}{dx}=\frac{x\lambda_{np}}{H_1}\left\lbrace e^{-x}-X_n(1+e^{-x}) \right\rbrace.
\end{align}

Using \eqref{eq:EQ22-21} we get $H_1=\left((4\pi ^3G\mathcal{Q}^4/45)\times 10.75\right)^{1/2}=1.13\text{sec}^{-1}$. The neutron-proton conversion rate $\lambda_{np}$ under our assumptions is given by \citep{Bernstein}
\begin{align}\label{eq:eq32-15}
\lambda_{np}=\frac{255}{\tau_n x^5}\left( 12+6x+x^2\right),
\end{align}
where $\tau_n =886.7\, \text{sec}$ is the life time of the neutron \citep{Dodelson}. It turns out that when $x=1$ or $T=\mathcal{Q}$, the conversion rate is $\lambda_{np}=5.5\,\text{sec}^{-1}$ which is greater than the expansion rate $H=1.13\, \text{sec}^{-1}$ implying that the conversion rate is efficient enough to maintain equilibrium. However, beneath $1\,\text{MeV}$, the conversion fades out. Indeed, integration of \eqref{eq:eq32-9} shows that neutrons froze out to a fraction of $X_n^f=0.15$ at $0.5\,\text{MeV}$ \citep{Bernstein}. Below $0.1\,\text{MeV}$, two reactions become important and these are deuterium production, $\text{n}+\text{p}\rightarrow \text{D}+\gamma$, and neutron decay, $\text{n}\rightarrow \text{p}+e^- +\bar{\nu}$.

Let us now call $T_{nuc}$ the instantaneous temperature at which light nuclei are produced and use it in \eqref{eq:eq32-4} whose we take the logarithm. This gives us
\begin{align}\label{eq:eq32-17}
\ln n_D-\ln n_b\sim \ln \eta_b +\frac{3}{2}\ln \left(\frac{T_{nuc}}{m_p} \right)+\frac{B_D}{T_{nuc}}.
\end{align}
If the world remained in equilibrium, all neutrons and protons would form deuterium. Therefore, $n_D\sim n_b$ and
\begin{align}\label{eq:eq32-18}
\ln \eta_b +\frac{3}{2}\ln \left(\frac{T_{nuc}}{m_p} \right)\sim-\frac{B_D}{T_{nuc}},
\end{align} 
which simply tells us that deuterium forms at $T_{nuc}\sim 0.07\,\text{MeV}$.
Using Friedmann equation for a  radiation-dominated universe where electrons and positrons have annihilated ($g_{\ast}=3.36$), we can get the time-temperature relation as \citep{Turner, Dodelson}
\begin{align}\label{eq:eq32-16}
t=132\,\text{sec}\left( \frac{0.1\text{MeV}}{T}  \right)^2.
\end{align}
The number of neutrons on the onset of nucleosynthesis can be calculated using result \eqref{eq:eq32-16} in the decay equation. This gives us
\begin{align}
X_n(T_{nuc})=X^f_ne^{-t/\tau_n}=0.15e^{-[(132/886.7)(0.1/0.07)^2]}=0.11.
\end{align}
We now consider the production of helium. After freeze-out, there occurred  a brief period of neutron decay. Since the binding energy of helium is larger than that of deuterium (see fig. \ref{fig:b}) all the  neutrons that survived the decay go into helium $^4\text{He}$ at $T\sim T_{nuc}$, but via deuterium. In fact, the exponential $e^{B/T}$ favours helium over deuterium \citep{Dodelson}. However, there is not enough energy allowing four-body reactions such as $\text{p}+\text{n}+\text{p}+\text{n}\longrightarrow\,^4\text{He}+ \gamma$ to take place. Instead, the reactions of the sort $\text{p}+\text{n}\longrightarrow \text{D}+\gamma$; $\text{D}+\text{p}\longrightarrow \, ^3\text{He}+\gamma$ and $\text{D}+\text{D}\longrightarrow\,^4\text{He}+\gamma$ take place first. Moreover, deuterium having the lowest binding energy of all nuclei, energetic photons prevent its formation until the temperature $T_{nuc}$ is of the order $0.1\,\text{Mev}$ and therefore the general big bang nucleosynthesis process is delayed. This is called deuterium bottleneck \citep{Gary}.

Now, the only baryons present are helium (helium nuclei) and hydrogen (protons). Since helium $^4\text{He}$ contains two protons and two neutrons, the number density of protons after helium production is $n_H=n_p-n_n$ and therefore the fractional mass of hydrogen (protons) is $X_H=(n_p-n_n)/(n_p+n_n)$. The mass fraction of helium is thus 
\begin{align}
Y=1-X_H=1-\frac{n_p-n_n}{n_p+n_n}=2\left(\frac{n_n}{n_n+n_p}\right)=2X_n=2\times 0.11=0.22.
\end{align}
There is agreement between this rough estimate and the exact analytic fit to the primordial mass fraction of $^4\text{He}$ \citep{Turner},
\begin{align}\label{eq:eq32-20}
Y=0.2262+0.0135\ln\left(10^{10}\eta_b\right).
\end{align}
Thus both $T_{nuc}$ and $Y$ depend only logarithmically on the baryon fraction. Finally, we have seen the reaction which change deuterium into helium at $T_{nuc}$. The rate of this reaction is not however strong enough to deplete deuterium which eventually freezes at the fractional mass $\sim 10^{-5}$. The other light nuclei formed are $^3\text{He}\sim 10^{-5}$, and $^7\text{Li}\sim 10^{-14} $. Our cosmic plasma contains now photons, electrons, neutrinos, protons, helium nuclei and some other light nuclei.

\begin{figure}
        \centering
        \begin{subfigure}{0.4840\textwidth}
                \includegraphics[width=\textwidth]{pics/nucleo}
                \caption{Light element abundances in the early universe}
          \label{fig:a}
        \end{subfigure}
         \quad
        \begin{subfigure}{0.4840\textwidth}
                \includegraphics[width=\textwidth]{pics/ben}
                \caption{Binding energy per nucleon}
                \label{fig:b}
        \end{subfigure}
       
        \caption{ On the left \eqref{fig:a}: Dashed curve results from integration of \eqref{eq:eq32-10}; light solid line is twice the neutron equilibrium abundance; heavy solid curves emerge from the exact solution code \citep{Wagoner}. Neutron fraction is in equilibrium until $\sim 1\,\text{MeV}$. The exact solution and equation \eqref{eq:eq32-10} agree until neutron decay. On the right \eqref{fig:b}: Among the light elements $^4\text{He}$ has the highest binding energy per nucleon. Nucleosynthesis stops at $^4\text{He}$ because of lack of tightly bound isotopes in the range $A=5-8$ \citep{Dodelson}. }
\end{figure}

\section{Recombination}
In the process of light element production in the early universe, the big bang nucleosynthesis as already discussed was followed by the process where the nuclei, or in other words the ionised atoms, captured free electrons to become neutral atoms  subsequently forming the corresponding elements. This is recombination. The vast majority of the nuclei which capture free electrons are in fact the free protons that survived the formation of helium. This explains why recombination is dominated by hydrogen formation, of course there is also helium recombination which even takes place earlier than that of hydrogen because of its greater ionisation potential.

Since the ionisation potential of hydrogen is $13.6\,\text{eV}$ we would expect the formation of neutral hydrogen at the temperature $13.6\,\text{eV}$ but until $\sim 1\,\text{eV}$ photons remain coupled to electrons via Compton scattering and electrons to protons via Coulomb scattering \citep{Dodelson}. Recombination  is thus  delayed by the high photon-to-baryon ratio and becomes only significant at $T\sim 0.3\,\text{eV}$. Before we discuss the real recombination, let us show that at $T=13.6\, \text{eV}$ all hydrogen is still ionised. We consider hydrogen formation in $e^-+\text{p}\longrightarrow \text{H}+\gamma$ for which \eqref{eq:eq31-24} leads to
\begin{align}\label{eq:eq32-21}
\frac{n_en_p}{n_H}=\frac{n_e^0n_p^0}{n_H^0}
\end{align}
which, by using results \eqref{eq:eq31-19}, leads to
\begin{align}\label{eq:eq32-22}
\frac{n_en_p}{n_H}=\left(\frac{m_eT}{2\pi} \right)^{3/2} e^{-\epsilon_0/T},
\end{align}
where we have assumed $m_p=m_H$ in the prefactor and  $\epsilon_0=m_e+m_p-m_H=13.6\,\text{eV}$ in the exponential is the hydrogen ionisation energy. As we did for the proton-neutron case, we define the free electron fraction as
\begin{align}\label{eq:eq32-23}
X_e\equiv \frac{n_e}{n_e+n_H}=\frac{n_p}{n_p+n_H},
\end{align} 
where we have used the neutrality of the universe condition, $n_e=n_p$. We can use \eqref{eq:eq32-22} and \eqref{eq:eq31-23} to obtain
\begin{align}\label{eq:eq32-24}
\frac{X_e^2}{1-X_e}=\frac{1}{n_e+n_H}\left\lbrace \left(\frac{m_eT}{2\pi} \right)^{3/2} e^{-\epsilon_0/T} \right\rbrace.
\end{align}
 We can neglect the small number of helium atoms and assume $n_p\,+n_H=n_b=\eta_b\,n_{\gamma}\sim 10^{-9}\,T^3$ as it follows from \eqref{eq:eqx}. Here we have used the fact that the number density of massless particles $\simeq T^3$ from \eqref{eq:eq31-20}. At $T=\epsilon_0$ as we said earlier the recombination is not yet effective, we have instead
\begin{align}\label{eq:eq32-25}
\frac{X_e^2}{1-X_e}\sim 10^9\left( \frac{m_e}{T}\right)^{3/2}\simeq 10^{15},
\end{align}
which can only be satisfied if $X_e$ is close to $1$, meaning that all the electrons are still free or all hydrogen is ionised, what we wanted to show. The Saha equation tells us more than this! If we define recombination as the point where $90\%$ of free electrons have found their nuclei to form neutral atoms or $X_e=0.1$, we can solve \eqref{eq:eq32-24} for $T_{rec}$ which we then use in the relation $1+z=T/T_0$ to estimate the redshift of recombination $z_{rec}$. This gives $T_{rec}=3575\,\text{K}=0.308\,\text{eV}$ and $z_{rec}=1300$.
 
In spite of this, we have to reject the equilibrium Saha equation for a while if we are to examine the out-of-equilibrium situation or what happens as $X_e$ falls and instead solve \eqref{eq:eq31-23} just as we did when we were calculating the neutron-proton ratio. The equation becomes
\begin{align}\label{eq:eq32-26}
\frac{dn_e}{dt}+3Hn_e=n_e^0n_p^0\langle \sigma v\rangle\left\lbrace \frac{n_H}{n_H^0}-\frac{n_e^2}{n_e^0n_p^0}\right\rbrace
\end{align}
which, on using equation \eqref{eq:eq32-24}, gives
\begin{align}\label{eq:eq32-27}
n_b\frac{dX_e}{dt}+X_e\frac{dn_b}{dt}+3Hn_bX_e=n_b\langle \sigma v\rangle\left\lbrace \left(1-X_e\right)\left(\frac{m_eT}{2\pi} \right)^{3/2} e^{-\epsilon_0/T} - X_e^2 n_b\right\rbrace.
\end{align}
Using that $n_b \propto a^{-3}$ and defining $x\equiv \epsilon_0/T$ as we did in the previous section, we can simplify our expression to

\begin{align}\label{eq:eq32-28}
\frac{dX_e}{dx}=\frac{\langle \sigma v\rangle}{xH}\left\lbrace \left(1-X_e\right)\left(\frac{m_e\epsilon_0}{2\pi x} \right)^{3/2} e^{-x} - X_e^2 n_b\right\rbrace.
\end{align}
Now let us call $H_1$ the Hubble rate at $\epsilon_0=T$. Taking the universe to be matter-dominated during recombination, we have $\rho\propto a^{-3}\propto T^3$, and therefore we can obtain $H=H_1x^{-3/2}$. Moreover, the recombination rate and photoionisation rate are given by $\langle \sigma v\rangle $ and $\beta\equiv \langle \sigma v\rangle \left(m_e\epsilon_0/2\pi x \right)^{3/2} e^{-x}$ respectively. All these allow us to simplify further our equation \eqref{eq:eq32-28} to
\begin{align}\label{eq:eq32-29}
\frac{dX_e}{dx}=\frac{\sqrt{x}}{H_1}\left[(1-X_e)\beta -  n_b X^2_e \langle \sigma v\rangle\right].
\end{align}


\begin{figure}
\center
\includegraphics[scale=0.61]{pics/recom}
\caption{Free electron as a function of redshift \citep{Dodelson}. Recombination occurs at $z\sim 1000$ corresponding to $T\sim 0.25\,\text{eV}$. The equilibrium Saha equation \eqref{eq:eq32-24} identifies only the redshift of recombination. Here $\Omega_b=0.06$, $\Omega_m=1$ and $h=1$.}
\end{figure}

The recombination rate  producing hydrogen in any state $n$ is a function of $E_n=\epsilon_0/n^2$ \citep{Turner}. However, only the recombination to an excited state ($n>1$) is what is relevant to producing neutral hydrogen. In fact, recombination to ground state ($n=1$) produces ionising photons which destroy then neutral atoms, producing no net recombination. A good approximation for recombination rate to excited states ($n=2,3,\cdots$) gives \citep{Dodelson}
\begin{align}\label{eq:eq32-31}
\langle \sigma v\rangle =9.78\frac{\alpha ^2}{m_e^2}\sqrt{x}\ln x. 
\end{align} 
Solving equation \eqref{eq:eq32-29} though not analytically easy can be made by realizing that at low temperatures or at large $x$ we can neglect the photoionisation term and then try to get the residual electron fraction which remains in the universe at $x=\infty$ after recombination by solving 
\begin{align}\label{eq:eq32-32}
\frac{dX_e}{dx}=-\lambda x\ln x X^2_e,
\end{align}
where $\lambda= 9.78n_b\alpha^2/m_e^2H_1$. Once again we change the variable, we introduce the redshift $z=T/T_0-1$, which leads to
\begin{align}
\frac{dX_e}{dz}=-\frac{\lambda\epsilon_0^2}{T_0^2} \left(z+1 \right)^{-3}  \ln \left(\frac{\epsilon_0}{T_0(z+1)}\right).
\end{align}
Evaluating this integral from $z=z_f$ to $z=0$ and then considering recombination continues for a so long time after freeze-out that $X_e ^f$ being very much larger than $X_e ^{\infty}$ we  can neglect $1/X_e ^f$,  we get the residual electron fraction $X_e^{\infty}$ as
\begin{align}\label{eq:fi}
X_e^{\infty}=\frac{2}{\lambda } \frac{\epsilon_0^2/T_0^2}{\ln(z_f+1)}.
\end{align}

Before computing the numerical value of the residual ionisation, let us first analyse what happens at electron freeze-out. At temperatures higher than $T_{rec}$, the rate of electron-photon interaction was greater than the expansion rate, photons were coupled to matter and were so energetic that they ionized any hydrogen atom that formed. The mean free path of photon was very short $\sim 1/n_e\sigma_T$ and the universe was opaque. As the universe expanded further, the photons lost energy because of redshift and therefore, became gradually unable to ionize neutral atoms being formed. As a result, after electrons were now part of neutral atoms, photons stopped interacting with matter, they became free and the universe was now transparent. This is photon decoupling which occurred effectively when the scattering rate equalled the expansion rate. These photons which decoupled at that time are what form the cosmic microwave background seen today at the temperature of $2.75\,\text{K}$. 

We can show that decoupling took place during recombination. Using the Thomson cross section $\sigma_T= 0.665\times 10^{-24}\,\text{cm}^2$, and using $m_pn_b/\rho_c=\Omega_ba^{-3}$, we can compute the rate of photon-electron  scattering $n_e\sigma_T$ as
\begin{align}\label{eq:eqaa}
\Gamma (z)= n_e\sigma_T = 7.477\times 10^{-29}\text{m}^{-1}X_e\Omega_b h^2 a^{-3}=7.477\times 10^{-29}\text{m}^{-1}X_e\Omega_b h^2 \left(1+z\right)^3
\end{align}
and its ratio to the expansion rate as
\begin{align}
 \frac{ n_e\sigma_T}{H} =0.0692\left(1+z\right)^3X_e\Omega_b h\frac{H_0}{H}.
\end{align}
The ratio $H_0/H$ in a matter-dominated flat universe is $\left(\Omega_{m}/a^3  \right)^{1/2}=\left[\Omega_{m}(1+z)^3\right]^{-1/2}$. Let us now normalize $1+z$ by $1000$, the baryon density by $0.02$ and the matter density by $0.15$, with this we can write
\begin{align}
\frac{n_e\sigma_T}{H}=113X_e\left(\frac{\Omega_bh^2}{0.02}\right)\left( \frac{0.15}{\Omega_m h^2}\right)^{0.5}\left( \frac{1+z}{1000}\right)^{1.5}.
\end{align}
We can see that when $X_e$ goes beneath $\sim 10^{-2}$, $H$ goes larger than $n_e\sigma_T$, photons decouple, implying that decoupling takes place during recombination since $X_e^{\infty}<10^{-2}$. When released at decoupling with a temperature $T_{dec}\sim 3000\,\text{K}$, the photons had more energy than today where they have $T_0=2.75\,\text{K}$ and therefore were not in the microwave range. 
The background has preserved its equilibrium energy distribution, the black body spectrum
\begin{align}
\epsilon(f)df = \frac{8\pi h}{ c^3}\frac{f^3 df }{ e^{h\nu/k_\mathrm{B}T} - 1}, 
\end{align}
because of two reasons. First, the redshift effect in the frequency $f$ is cancelled by that in the temperature $T$ in the denominator since both scale as $a^{-1}$ and appear only in $f/T$. Second, in the numerator $f^3$ scales as $a^{-3}$ and on the left hand side, the energy density $\epsilon(f)$ scales as $a^{-3}$, resulting in no net effect. This is why the CMB radiation as seen today is still a black body spectrum with a peak in the microwave region.

We can now carry on with computing $X_e^{\infty}$.  We can use the ratio $H^2/H_0^2$ in the case of a matter-dominated flat universe to compute $H_1$. In fact, we can show that $H=\Omega_m^{1/2}a^{-3/2}H_0=\Omega_m^{1/2}(1+z)^{3/2}H_0=\Omega_m^{1/2}\left( T/T_0\right)^{3/2}H_0$. Therefore, to compute $H_1$ we just set $T$ equal to $\epsilon_0$ and get $H_1=\Omega_m^{1/2}\left( \epsilon_0/T_0\right)^{3/2}H_0$. We use today's values of parameters $\Omega_m=0.315$, $\Omega_bh^2=0.0221$, $h=0.67$, the fine structure constant $\alpha = 7.3\times 10^{-3}$, $\epsilon_0=13.6\times 1.6\times10^{-19}\,\text{J}$, $T_0=2.43\times 1.6\times 10^{-19}\,\text{J}$, $\rho_c=1.88h^2\times 10^{-26}\,\text{kg}\,\text{m}^{-3}$ and $H_0=100,000\,h\,\text{m}\times\left(3.08\times 10^{22}\,\text{m\,sec}\right)^{-1}$.

The analysis above has shown us that decoupling takes place when $X_e\sim 10^{-2}$ during recombination. We can use this to compute the scattering rate at decoupling and then we solve $H(z_f)=\Gamma(z_f)$ for $z_f$. Using \eqref{eq:eqaa} and the expression for $H(Z)$ above, we get
\begin{align}\label{eq:eqz}
H_0(1+z_f)^{3/2}\,\Omega_m^{1/2}=7.477\times 10^{-28}\text{m}^{-1}X_e\Omega_b h^2 \left(1+z_f\right)^3.
\end{align}
To give sense to this equality, we use speed of light $c$ to change the unit on its right hand  side of to $\text{sec}$ because the unit of $H$ on the left hand side turns out to be $\text{sec}$. This leads to
\begin{align}
1.235 \times 10^{-18}(1+z_f)^{3/2}=4.4\times 10^{-21}X_e(z_f)(1+z_f)^3
\end{align}
or \begin{align}\label{eq:eqdec}
1+z_f=\frac{43}{\left(  X_e(z_f)\right)^{2/3}}.
\end{align}
For $X_e(z_f)=1/113=8.85\times 10^{-3}$, we get $1+z_f=1005$. A better analysis using the visibility function shows however that photons decoupled at $1+z_f\sim 1100$. Finally, we use this value, together with the values for different constants written above, in \eqref{eq:fi} to compute roughly the residual ionisation, which gives $X_e^{\infty} \simeq 10^{-3}$.

\section{Freeze-out of Dark Matter}
As we said earlier, the most likely candidate in the search for dark matter particle is a weakly interacting particle (WIMP). We want to use the Boltzmann equation to show that the relic abundance of the WIMP is $\Omega_{dm}=0.3$. We consider that before freeze-out, the equilibrium interaction consists of two heavy particles (two WIMPs) that annihilate to produce two massless leptons $l$. Let $m$ and $n_w$ be the mass and the number density of the WIMP.  We can, as usual, write
\begin{align}\label{eq:eq32-40}
\frac{dn_w}{dt}+3Hn_w=\langle \sigma v\rangle\left\lbrace (n_w^0)^2-(n_w)^2\right\rbrace.
\end{align}
The fact that temperature $T$ scales as $a^{-1}$ serves us again. We define $Y\equiv n_w/T^3$ and $Y_{eq}=n^0_w/T^3$ which we use to change equation \eqref{eq:eq32-40} to
\begin{align}\label{eq:eq32-41}
\frac{dY}{dt}=T^3\langle \sigma v\rangle\left\lbrace (Y_{eq}^2-Y^2\right\rbrace.
\end{align}
We apply the same trick of changing the time variable, we define $x\equiv m/T$. Equilibrium requires high temperatures, $x\ll1$ and the  particles behave relativistically and can therefore be taken as massless, $m\ll T$. This implies $Y\simeq 1\simeq Y_{eq}$. When the temperature falls beneath $m$ or, in other words, for high $x$, the interactions are not efficient enough to maintain equilibrium; the particles will then freeze out. In radiation-dominated universe we have $H = H_1/x^2$, with $H_1=H_{m=T}$. Working with the variable $x$ rather than $t$ allows us to write
\begin{align}\label{eq:eq32-42}
\frac{dY}{dx}=-\frac{m^3}{Hx^4}\langle \sigma v\rangle\left\lbrace (Y^2-Y_{eq}^2\right\rbrace.
\end{align}
Defining the ratio of the annihilation rate to the expansion rate when $m=T$ by $\lambda \equiv m^3\langle \sigma v\rangle/H_1$, we simplify further our equation to
 \begin{align}\label{eq:eq32-43a}
 \frac{dY}{dx}=-\frac{\lambda}{x^2}\left\lbrace (Y^2-Y_{eq}^2\right\rbrace.
\end{align}



\begin{figure}
\center
\includegraphics[scale=0.6]{pics/wimp}
\caption{Abundance of the WIMP as $T$ drops beneath its mass \citep{Dodelson}. Dashed curve is equilibrium abundance. Two solid lines indicate abundance for two different values of $\lambda$. Difference between Botzmann statistcs and quantum statistic is important at $T$ larger than $m$.}
\end{figure}

Although \eqref{eq:eq32-43a} has no analytical solution, we can still use it to determine the the value of $Y$ after freeze-out. We start by realizing that when $m=T$ or $x=1$, the left  and the right hand sides of, \eqref{eq:eq32-43a} are of the orders $Y$ and $\lambda Y^2$ respectively. Since $\lambda$ is typically large, the right hand side must vanish as long as $Y$ is not too small. This just means $Y=Y_{eq}$ at $x\leq 1$ in accordance to what we said earlier. However, as the universe expands, the temperature drops and $Y_{eq}$ fall exponentially. Therefore, at a certain temperature, our WIMP will not be able to annihilate fast enough to maintain equilibrium, it will freeze out. At late times or when $x\gg 1$, we can write
\begin{align}\label{eq:eq32-43}
 \frac{dY}{dx}=-\frac{\lambda}{x^2}Y^2
\end{align}
which, on integrating from the freeze-out time $x_f$ to a very late time $x=\infty$, yields
\begin{align}\label{eq:eq32-44}
\frac{1}{Y_{\infty}}-\frac{1}{Y_f}=\frac{\lambda}{x_f}.
\end{align}
Assuming that the annihilation of the WIMPs into leptons continues for a so long time after freeze-out that $Y_f$ being  much larger than $Y_{\infty}$, we can neglect $1/Y_f$. This leads us to
\begin{align}
Y_{\infty}=\frac{x_f}{\lambda}=\frac{H_1}{\langle \sigma v\rangle m^2T_f}.
\end{align}

When $Y$ has reached its constant value $Y_{\infty}$,  the number density at this sufficiently late time is $Y_{\infty}T^3_1$. since  the particle number density scales as $a^{-3}$, the energy density today can be written as
\begin{align}
\rho_w = mY_{\infty}T^3_0\left(\frac{a_1T^3_1}{a_0T^3_0} \right)=\frac{H_1}{\langle \sigma v\rangle mT_f}T_0^3\left(\frac{a_1T^3_1}{a_0T^3_0} \right).
\end{align}
From our definition of entropy density as $s\equiv (\rho + P)/T$ in \eqref{eq:eqs}, we can infer that the product $(sa)^3$ remains constant,  allowing us to write $(s_1a_1)^3=(s_0a_0)^3$ or equivalently,
\begin{align}
\left[ g_{\ast}(aT)^3\right]_{T=T_1}=\left[g_{\ast}(aT)^3\right]_{T=T_0}.
\end{align}
At high temperatures, the entropy in the cosmic plasma comes from fermions: quarks, antiquarks, leptons and antileptons, and bosons: photons and gluons. Realizing the top quark does not contribute because it is too heavy, the effective number of degrees of freedom can be computed as $g_{\ast}=91.5$ \citep{Dodelson}. Today, the entropy comes only from neutrinos and photons and therefore, $g_{\ast}=3.36$. Using this assumption together with \eqref{eq:eqs} and \eqref{eq:eqr}, we can show that $(a_1T_1)^3/(a_0T_0)^3\simeq 1/30$. Using this result, we write
\begin{align}
\rho_w=\frac{H_1T_0^3}{30\langle \sigma v\rangle mT_f}.
\end{align}
We recall that $H_1$ is the expansion rate when the temperature equaled the WIMP mass $m$, therefore $H_1=\left( 4\pi^3 Gg_{\ast}/45\right)^{1/2}$. Then, the density parameter for the WIMP is 
\begin{align}
\Omega_w=\left(\frac{4\pi^3 Gg_{\ast}}{45}\right)^{1/2}\frac{x_fT_0^3}{30\langle \sigma v\rangle\rho_{cr}}.
\end{align}
Furthermore, the production of the WIMP takes place at a high temperature where all the particles of the standard model are relativistic so that  $g_{\ast}$ scales as $100$. Using the values of the constants and  conveniently normalizing $x_f$ by $10$, we get
\begin{align}
\Omega_w= \frac{0.3}{h^2}\frac{x_f}{10}\left(\frac{g_{\ast}}{100}\right)^{1/2}\frac{10^{-39}\text{cm}^2}{\langle \sigma v\rangle},
\end{align}
which is promising since the WIMP, being weakly interacting, should have a cross section of the order $10^{-39}\,\text{cm}^2$.

