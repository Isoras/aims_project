\chapter{Overview of Boltzmann Equation}

In the present chapter we want to recall the meaning of the phase space distribution function of particles and then obtain the relativistic Boltzmann equation it satisfies. The Boltzmann equation is used to study different out-of-equilibrium phenomena involving particles. By making use of some practical cosmological features, we also derive Saha equation from the Boltzmann one. The Saha equation is useful when studying equilibrium interactions of particles. And, indeed these two equations  are the only tools we need in the fourth chapter. 

\section{Distribution Function}
The central piece of the third chapter is the Boltzmann equation. It makes sense however to first look at the distribution function which satisfies it. Indeed, the distribution function $f_i$  is a 7-D function of time $t$, coordinates $x^{\mu}$ and  momenta $p^{\mu}$; it measures the average particle number density in phase space or position-momentum space. That is, $f_i(x^{\mu},p^{\mu})d^3xd^3p$ is the number of particles of species $i$ that are found in a small but finite volume element $d^3x$ around the point $x^{\mu}$ and have momenta in the vicinity of  $p^{\mu}$ \citep{Dodelson}. In our definition, we are talking about proper momentum, not comoving momentum, which falls as $a^{-1}$.

With the help of the distribution function, we can compute various macroscopic properties of systems of particles. In the case of a spatially homogeneous and isotropic distribution, the distribution function obviously depends on the three-momentum and time. We can therefore obtain the number density, energy density and pressure for species $i$ particles as
\begin{align}\label{eq:eq31-0}
n_i=g_i\int \frac{d^3p}{(2\pi)^3}f(p,t);\quad \rho= g_i\int\frac{d^3p}{(2\pi)^3}f_i(p,t)E(p);\quad P_i= g_i\int\frac{d^3p}{(2\pi)^33E(p)} f_i(p,t)p^2 
\end{align}
respectively.
where, according to quantum considerations, $g_i$ is the number of degrees of freedom of the particle of species $i$  and $(2\pi \hbar)^3$ rises due to the Heisenberg uncertainty principle setting the unit phase space volume element to $d^3p/(2\pi\hbar )^3$. For particles in thermodynamic equilibrium, the function $f$ is the Bose-Einstein/Fermi-Dirac distribution function given by
\begin{align}\label{eq:eq31-0a}
f_i(E)=\dfrac{1}{e^{(E_i-\mu_i)/T}\pm 1}
\end{align}
where the positive sign works for fermions and the negative one for bosons. In case of chemical equilibrium, the chemical potentials $\mu_i$ of interacting particles are related. For example, when a particle of type $a$ interacts with particles of species $b$, $c$  and $d$ according to $a+b \longleftrightarrow c+d$, we can write
\begin{align}
\mu_a+\mu_b=\mu_c+\mu_d.
\end{align}
One can show that for cases where the chemical potential is much smaller than the temperature, as it is in cosmology, the distribution function depends only on $E/T$ and the pressure satisfies 
\begin{align}
\frac{\partial P_i}{\partial T}=\frac{\rho_i + P_i}{T},
\end{align}
which can be used also to show that the entropy density is given by
\begin{align}\label{eq:eqs}
s = \frac{\rho + P}{T}.
\end{align}
In the radiation-dominated universe, the contribution to energy density and pressure  come from the relativistic particles ($m_i\ll T$). We can compute them to obtain
\begin{align}\label{eq:eqr}
\rho_{rad} =\frac{\pi^2}{30}g_{\ast}T^4; \quad
P_{rad}=\rho_{rad}/3=\frac{\pi^2}{90}g_{\ast}T^4,
\end{align}
where $g_{\ast}$ is the effective number of degrees of freedom of the contributing particles, given by \citep{Turner}
\begin{align}
g_{\ast}=\sum_{i=boson}g_i\left( \frac{T_i}{T}\right)^4+\frac{7}{8}\sum_{i=fermion}g_i\left( \frac{T_i}{T}\right)^4.
\end{align}
\section{Boltzmann Equation}
This is the equation that governs the evolution in phase space of the distribution function discussed above and it simply says the number of particles in a phase space volume element is constant unless there are collisions. It can be written as
\begin{align}\label{eq:eq31-4}
\hat{\bold{L}}[f]=\bold{C}[f],
\end{align}
where  $C$ is collision operator and $\hat{\bold{L}}$ is the Liouville operator for the phase space distribution function $f$. For a species of particles of mass $m$ acted upon by an external force $\bold{F}=\bold{p}/dt$, the nonrelativistic Liouville's operator is 
\begin{align}\label{eq:eq31-5}
    \hat{\mathbf{L}} = \frac{\partial}{\partial t} + \frac{\mathbf{p}}{m} \cdot \nabla + \mathbf{F}\cdot\frac{\partial}{\partial \mathbf{p}}\,. 
\end{align}
We are however interested in a relativistic version of the Liouville operator. The covariant relativistic Liouville operator is written as
\begin{align}\label{eq:eq31-6}
 \hat{\mathbf{L}}=p^{\mu}\frac{\partial}{x^{\mu}}-\Gamma^{\mu}_{\beta\nu}p^{\beta}p^{\nu}\frac{\partial}{\partial p^{\mu}},
\end{align}
where the connection $\Gamma^{\mu}_{\beta\nu}$ introduces the curvature of spacetime and therefore, the gravity.
For a spatially homogeneous and isotropic universe, the Liouville operator for $f_i=f_i(p,t)=f_i(E,t)$ reduces to the zeroth component of the operator in \eqref{eq:eq31-6} which turns out to be

\begin{align}\label{eq:eq31-7}
 \hat{\mathbf{L}}=E\frac{\partial}{\partial t}-\frac{\dot{a}}{a}p^{2}\frac{\partial}{\partial E}.
\end{align}

The relativistic Boltzmann equation that governs the evolution of $f(E,t)$ in phase space can then be written as
\begin{align}\label{eq:eq31-8}
E\frac{\partial f_i}{\partial t}-Hp^{2}\frac{\partial f_i}{\partial E}=\bold{C}[f_i],
\end{align}
where $H=\dot{a}/a$ is the Hubble parameter.
On integrating by parts the Boltzmann equation \eqref{eq:eq31-8} over momentum and then use \eqref{eq:eq31-0}, we get
\begin{align}\label{eq:eq31-10}
\frac{dn_i}{dt}+3Hn_i=\frac{1}{(2\pi)^3}\int \bold{C}[f_i]\frac{d^3p}{E} \quad\text{or}\quad a^{-3}\frac{d(a^3n_i)}{dt}=\frac{1}{(2\pi)^3}\int \bold{C}[f_i]\frac{d^3p}{E}.
\end{align}
It turns out that in the expanding universe  the physical number density of particles falls off as $a^{-3}$. The evaluation of the collision integral on the right hand side of \eqref{eq:eq31-10} depends on the nature of particle interactions. Let us now examine the situation where the number density $n_a$ of species $a$ particles is affected only by annihilation with species $b$ producing two particles $c$ and $d$, shown schematically as $a+b\longleftrightarrow c+d$. The double arrow shows that inverse process can  produce $c$ and $d$. The Boltzmann equation for this equation is
\begin{align}\label{eq:eq31-11}
 \frac{dn_a}{dt}+3Hn_a&=\int \frac{d^3p_a}{(2\pi)^32E_a}\int  \frac{d^3p_b}{(2\pi)^32E_b}\int  \frac{d^3p_c}{(2\pi)^32E_c}\int  \frac{d^3p_d}{(2\pi)^32E_d}\times A
\end{align}
with
\begin{align}\label{eq:eq31-12}
A=(2\pi)^4\delta^3(p_a+p_b-p_c-p_d)\delta(E_a+E_b-E_c-E_d)\vert \mathcal{M} \vert ^2 \times S,
\end{align}
where we have, in addition to the delta functions of energies and momenta enforcing the conservation of energy and momentum, the amplitude $\mathcal{M}$ determined from the fundamental physics of interaction in question and the quantity $S$ defined as
\begin{align}\label{eq:eq31-13}
S=\{ f_cf_d(1\pm f_a)(1\pm f_b)-f_af_b(1\pm f_c)(1\pm f_d)\}.
\end{align}
In $S$ the production term proportional to $f_cf_d$ and the loss term proportional to $f_af_b$ both contain a quantity $1\pm f_i$ representing Pauli blocking/Bose enhancement effects where the plus sign works for bosons and the minus for fermions.

 
We now have recourse to simplifying features and assumptions. First, we realize that rapid scattering processes enforce a kinetic equilibrium so that the distributions for the interacting particles retain the Fermi-Dirac (FD)/Bose-Einstein (BE) distribution functions. Therefore, $f_i$, with $i=a,b,c,d$, in \eqref{eq:eq31-13} is the BE/FD  distribution function (occupation number) of particles of species $i$.  
Second, since we want to study out-of-equilibrium phenomena, the system is not in chemical equilibrium but at least the assumed kinetic equilibrium will reduce the problem to solving a single ordinary differential equation for the chemical potential $\mu_i(T)$ or $\mu_i(t)$ rather than the complicated Boltzmann equation \eqref{eq:eq31-11}

Finally, we will be interested in systems at temperatures $T$ smaller than $(E_i-\mu_i)$. This reduces the quantum BE/FD  distribution \eqref{eq:eq31-0a} to the classical Boltzmann distribution
\begin{align}\label{eq:eq31-15}
f_i(E)=e^{-(E_i-\mu_i )/T}.
\end{align}
But also, $ f_i +1\approx 1 $. Making use of our simplifications, we can reduce expression \eqref{eq:eq31-13} for $S$ to
\begin{align}\label{eq:eq31-17}
S=e^{-(E_a+E_b)/T}\{ e^{(\mu_c+\mu_d)/T}-e^{(\mu_a+\mu_b)/T} \}.
\end{align}
Using \eqref{eq:eq31-15}, we can write \eqref{eq:eq31-0} for the time-dependent number density as
\begin{align}\label{eq:eq31-18}
n_i(t)=\frac{g_ie^{\mu_i /T}}{2\pi ^2} \int p^2e^{-E_i/T}dp.
\end{align}
Furthermore, since $n_i(t)$ is a function of $\mu_i(t)$  we can now reset our problem, instead of solving for $\mu_i$ we solve for $n_i$. To  proceed, let us define the species-dependent equilibrium ($\mu=0$) number density as
\begin{align}\label{eq:eq31-19}
n_i^0\equiv \frac{g_i}{2\pi^2}\int p^2e^{-E_i/T}dp
\end{align}
which, upon integration, yields
\begin{align}\label{eq:eq31-20}
n_i^0=g_i\left(\frac{m_iT}{2\pi} \right)^{3/2}e^{-m_i/T}\quad\text{and}\quad
n_i^0=g_i\frac{T^3}{\pi^3} 
\end{align}
for the nonrelativistic case, $m_i\gg T$, and the relativistic one, $m_i\ll T$, respectively.
Using \eqref{eq:eq31-18} and \eqref{eq:eq31-19}, we can write $e^{\mu_i/T}=n_i/n_i^0$. Using this result we can transform \eqref{eq:eq31-17} into
\begin{align}\label{eq:eq31-21}
S=e^{-(E_a+E_b)/T}\left\lbrace \frac{n_cn_d}{n_c^0n_d^0}-\frac{n_an_b}{n_a^0n_b^0}\right\rbrace.
\end{align}
We now define the thermally averaged cross section as
\begin{align}\label{eq:eq31-22}
\langle \sigma v\rangle=\frac{1}{ n_a^0n_b^0}\int \frac{d^3p_a}{(2\pi)^32E_a}\int  \frac{d^3p_b}{(2\pi)^32E_b}\int  \frac{d^3p_c}{(2\pi)^32E_c}\int  \frac{d^3p_d}{(2\pi)^32E_d}\times A.
\end{align}
Using \eqref{eq:eq31-22} and \eqref{eq:eq31-21} into equation \eqref{eq:eq31-11}, we obtain a simplified Boltzmann equation
\begin{align}\label{eq:eq31-23}
\frac{dn_a}{dt}+3Hn_a=n_a^0n_b^0\langle \sigma v\rangle\left\lbrace \frac{n_cn_d}{n_c^0n_d^0}-\frac{n_an_b}{n_a^0n_b^0}\right\rbrace.
\end{align}

We need to do something further to get  the final form of Boltzmann equation we want. The cosmological time is $H^{-1}$ and therefore the left hand side of \eqref{eq:eq31-23} is of the order $n_a/t$ or $Hn_a$. The right hand side of the same equation is of the order $n_an_b\langle \sigma v\rangle$. We therefore realize that if the reaction rate $\Gamma_b= n_b\langle \sigma v\rangle$ is much larger than the expansion rate $H$ as it was in the early universe, then to keep equality in \eqref{eq:eq31-23} the content in the braces must vanish which finally lead us to the famous Saha equation,
\begin{align}\label{eq:eq31-24}
\frac{n_cn_d}{n_c^0n_d^0}=\frac{n_an_b}{n_a^0n_b^0}.
\end{align} 
We will use this equation to describe the equilibrium part of our next discussion.
